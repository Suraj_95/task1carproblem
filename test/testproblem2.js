const data = require("../data");
const problem2 = require("../problem2");
const result = problem2(data);
if (result.length == 0) {
  console.log(result);
} else {
  console.log(
    `Last Car made is a ${result["car_make"]} and its model is ${result["car_model"]}`
  );
}
