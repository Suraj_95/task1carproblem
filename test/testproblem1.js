const data = require("../data");
const problem1 = require("../problem1");
var n = 33;
const result = problem1(data, n);
if (result.length == 0) {
  console.log(result);
} else {
  console.log(
    `Car with id ${result["id"]} is of year ${result["car_year"]}  made as ${result["car_make"]} and it's mode is ${result["car_model"]}`
  );
}
