function problem5(d) {
  if (d === undefined || d.length === 0) return [];
  else {
    let yearArr = [];
    for (let i = 0; i < d.length; i++) {
      if (d[i]["car_year"] < 2000) {
        yearArr.push(d[i]);
      }
    }
    return yearArr;
  }
}

module.exports = problem5;
