function problem4(d) {
  if (d === undefined || d.length === 0) return [];
  else {
    let car_years = [];
    for (let i = 0; i < d.length; i++) {
      car_years[i] = d[i]["car_year"];
    }
    return car_years;
  }
}

module.exports = problem4;
