function problem3(d) {
  if (d === undefined || d.length === 0) return [];
  else {
    let car_models = [];
    for (let i = 0; i < d.length; i++) {
      car_models[i] = d[i]["car_model"].toLowerCase();
    }
    car_models.sort();
    return car_models;
  }
}

module.exports = problem3;
