function problem2(d) {
  if (d === undefined || d.length === 0) return [];
  else {
    let lastCar = d.length - 1;
    return d[lastCar];
  }
}

module.exports = problem2;
