function problem1(d, n) {
  if (n === undefined) {
    return [];
  }
  if (d === undefined || d.length === 0) return [];
  else {
    for (let i = 0; i < d.length; i++) {
      if (d[i]["id"] === n) {
        return d[i];
      }
    }
    return [];
  }
}

module.exports = problem1;
