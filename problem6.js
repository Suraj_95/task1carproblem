function problem6(d) {
  if (d === undefined || d.length === 0) return [];
  else {
    let bmw_audi = [];
    for (let i = 0; i < d.length; i++) {
      if (d[i]["car_make"] == "Audi" || d[i]["car_make"] == "BMW")
        bmw_audi.push(d[i]);
    }
    return JSON.stringify(bmw_audi);
  }
}

module.exports = problem6;
